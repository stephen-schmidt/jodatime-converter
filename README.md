# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick little java app to convert jodatime longs into a readable date string
* 1.0

### How do I get set up? ###

* Open the project in intelliJ
* Build to pull down dependencies
* Run

### Usage ###
* Copy paste jodaTime longs from Json into the command line of the running app and hit enter. 
* Repeat as long as you want
* Enter -1 to quit