package com.fourtwentyconnect;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner s =  new Scanner(System.in);
        while(s.hasNext()) {
            String input = s.next();
            try{
                Long l = Long.parseLong(input);
                if (l == -1) {
                    break;
                }
                printDate(l);
            }catch (Exception e) {
                System.out.println("Not a valid long");
            }

        }
    }

    static private void printDate(Long jodaTime) {
        DateTime d= new DateTime(jodaTime);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("MM/dd/yyyy hh:mm a z");
        System.out.println(fmt.print(d));
    }
}
